package it.unibo.oop.lab.exception2;

public class NotEnoughFoundsException extends IllegalStateException{
	
	public String getMessage() {
		return "Not enough money for a draw operation to complete!";
	}

}
