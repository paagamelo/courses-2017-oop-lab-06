package it.unibo.oop.lab.exception2;

public class TransactionsOverQuotaException extends IllegalStateException{
	
	public String getMessage() {
		return "The number of your ATM transactions is over the maximum allowed!";
	}
	
}
