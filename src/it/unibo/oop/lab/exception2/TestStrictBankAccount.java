package it.unibo.oop.lab.exception2;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * JUnit test to test
 * {@link StrictBankAccount}.
 * 
 */
public class TestStrictBankAccount {

    /**
     * Used to test Exceptions on {@link StrictBankAccount}.
     */
    @Test
    public void testBankOperations() {
        /*
         * 1) Creare due StrictBankAccountImpl assegnati a due AccountHolder a
         * scelta, con ammontare iniziale pari a 10000 e nMaxATMTransactions=10
         * 
         * 2) Effetture un numero di operazioni a piacere per verificare che le
         * eccezioni create vengano lanciate soltanto quando opportuno, cioè in
         * presenza di un id utente errato, oppure al superamento del numero di
         * operazioni ATM gratuite.
         */
    	
    		StrictBankAccount acc1 = new StrictBankAccount(1, 10000, 10);		//Rich man
    		StrictBankAccount acc2 = new StrictBankAccount(2, 10000, 10);		//Arsenio Lupin
    		
    		//Lupin tries to drain the rich man BankAccount
    		try {
    			acc1.withdraw(2, 10000);
    			fail();
    		} catch(WrongAccountHolderException e) {
    			assertNotNull(e.getMessage());
    		}
    		
    		//Lupin tries to withdraw lot more than he owns
    		try {
    			acc2.withdraw(2, 15000);
    			fail();
    		} catch(NotEnoughFoundsException e) {
    			assertNotNull(e.getMessage());
    		}
    			
    		//Rich man tries to withdraw from ATM too much times than allowed
    		try {
    			for(int i = 0; i <= 10; i++) {
    				acc1.withdrawFromATM(1, 100);
    			}
    			fail();
    		} catch(TransactionsOverQuotaException e) {
    			assertNotNull(e.getMessage());
    		}
    }
}
