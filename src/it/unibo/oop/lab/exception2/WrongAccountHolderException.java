package it.unibo.oop.lab.exception2;

public class WrongAccountHolderException extends IllegalStateException{
	
	public String getMessage() {
		return "Unhautorized user!";
	}

}
