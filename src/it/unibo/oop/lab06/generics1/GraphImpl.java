package it.unibo.oop.lab06.generics1;

import java.util.*;

public class GraphImpl<N> implements Graph<N>{
	
	private Map<N,Set<N>> nodes;

	public GraphImpl() {
		this.nodes = new HashMap<>();
	}
	
	public void addNode(N node) {
		nodes.put(node, new HashSet<>());
	}

	public void addEdge(N source, N target) {
		nodes.get(source).add(target);
	}

	public Set<N> nodeSet() {
		return new HashSet<>(nodes.keySet());
	}

	public Set<N> linkedNodes(N node) {
		return new HashSet<>(nodes.get(node));
	}

	public List<N> getPath(N source, N target) {
		ArrayList<N> path = new ArrayList<>();
		path.add(source);
		
		HashMap<N,Boolean> visited = new HashMap<>();
		for(N node : nodeSet()) {
			visited.put(node, false);
		}
		
		return buildPath(source, target, path, visited);
	}
	
	private List<N> buildPath(N source, N target, List<N> path, Map<N,Boolean> visited) {
		if(source.equals(target)) {
			return path;
		}
		
		Iterator<N> iter = linkedNodes(source).iterator();		
		
		if(!iter.hasNext()) {
			this.backTrack(source, target, path, visited);
			return path;
		} else {
			N nextNode = iter.next();
			do {
				if(!path.contains(nextNode)) {
					path.add(nextNode);	
					buildPath(nextNode, target, path, visited);
					return path;
				}
				nextNode = iter.next();
			} while(iter.hasNext() && !visited.get(nextNode));
			this.backTrack(source, target, path, visited);
			return path;
		}
	}
	
	private void backTrack(N source, N target, List<N> path, Map<N,Boolean> visited) {
		path.remove(source);
		N previousNode = path.get(path.size() - 1);
		
		visited.replace(source, true);
		
		buildPath(previousNode, target, path, visited);
	}
}
